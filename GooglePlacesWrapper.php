<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GooglePlacesWrapper
 * Class to wrap the places api funcionality to PHP
 * using the api web requestsq
 * 
 * @author jeffom
 */
class GooglePlacesWrapper {
    //put your code here
    //https://maps.googleapis.com/maps/api/place/search/json?
    const REQUEST_URL = "https://maps.googleapis.com/maps/api/place/textsearch/";    
    const API_KEY = 'AIzaSyBioRfM8AI9mJpC233Ll1645Iu4SwqZ4jE';
    const MAPS_URL = "http://maps.google.com/maps/api/geocode/json?";
    const MAX_REQUESTS = 3;
        
    private $requestCount = 0;
    
    /**
     * Fetches the places based on the query 
     * @param type $query the query i.e:'Cheese in Paris'
     * @param type $next_page_token used only if theres a token to fetch the next results
     * @param array $result return array reference, this is used to add data to the return array     
     * @return array return an array with the data if sucessfull else it'll return with the status     
     */
    function request_places_by_query($query, $next_page_token=null,
            &$places_result = null)
    {        
        $this->requestCount = 0;
        $url = self::REQUEST_URL;
        //building the request url
        $url .= "json?";
        $url .= "query=".urlencode($query);
        $url .= "&";        
        $url .= "sensor=false";
        $url .= "&";        
        $url .= "key=".self::API_KEY;
        
        //getting the next results
        if( $next_page_token != null)
        {
            $url .= "&";        
            $url .= "pagetoken=$next_page_token";
        }       
        
        $response = file_get_contents( $url );
                
        if( $response )
        {
            $response = json_decode($response);
            
            if( $response->status == "OK")
            {
                //request was successfull
                //build return data
                
                $results = $response->results;
                
                if( $places_result == null )
                    $places_result = array();
                
                foreach($results as $p)
                {
                    $place = array('name'=>$p->name, 'address'=>$p->formatted_address);
                    $places_result[] = $place;
                }

                if(isset( $response->next_page_token) && $response->next_page_token )
                {
                    $fallback_results = $places_result;
                    try
                    {
                        //asserting so we don't overflow the server with requests
                        //in case of some unexpected error
                        if($this->requestCount < self::MAX_REQUESTS)
                        {
                            //we need to wait for while to make our token valid                            
                            sleep(1);
                            $this->request_places_by_query($query, $response->next_page_token, 
                                $places_result);
                        }
                        $this->requestCount++;
                    }
                    catch(Exception $e)
                    {                        
                        $places_result = $fallback_results;
                    }
                }

                //return $places_result;
                
                $return_data = array();
                $return_data['success'] = '1';
                $return_data['result'] = $places_result;
            }
            else
            {
                $return_data = array();
                $return_data['success'] = '0';
                $return_data['result'] = $response->status;     
                //throw new Exception($response->status);                
            }
            
            return $return_data;
        } 
        else
        {
            return null;
        }
    }
        
    /**
     * Google apis test
     * Try to get the place coordinates to send the correct web request
     * @param type $place the place name (like Berlin, Tokyo and etc...)
     * @return mixed return the location array if successfull else returns null
     */
    static function request_coordinates($place)
    {
        //building the url
        $url = self::MAPS_URL;
        $url .= "address=".urlencode("$place");
        $url .= "&";
        $url .= "sensor=false";        
        
        $response = file_get_contents($url);
        $response = json_decode($response, true);

        if(     $response && 
                isset( $response['results'] ) &&
                isset( $response['results'][0]['geometry'] )
        )
        {   
            //getting the first valid location (we're kinda of flying blind here)
            $lat = $response['results'][0]['geometry']['location']['lat'];
            $long = $response['results'][0]['geometry']['location']['lng'];
            return array("lat"=>$lat, "long"=>$long);
            
        }
        else 
        {            
            return null;
        }
    }
}
?>

<?php

//checking our function key
if( isset( $_GET['requestPlace'] ))
{
    $query = '';
    
    if( isset($_GET['query']) )
    {
        $query = $_GET['query'];
    }
    
    foo($query);
}

/**
 * Function to starting the data retrieving process
 * @param type $query
 */
function foo($query)
{
    include_once 'GooglePlacesWrapper.php';
    
    //removing 1st param from the get requests (that's our function name)
    $params = array_slice($_GET, 1);           
    
    try
    {
        $gp = new GooglePlacesWrapper();
        $result = $gp->request_places_by_query($query);    
        
        echo "total_results:".count($result['result']);   
        echo "<br>";
        echo json_encode($result['result']);
        echo "<br>";
    }
    catch(Exception $e)
    {
        echo($e->getMessage());
    }
    
    //echo json_encode($coords);
}

?>
